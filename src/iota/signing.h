#ifndef SIGNING_H
#define SIGNING_H

#include "stdbool.h"
#include "iota_types.h"
#include "transaction.h"

class Signing {
private:
	uint8_t m_state[48];
	uint32_t m_addressIndex;
	Keccak384* m_keccak;

protected:
public:
	Signing(Keccak384* keccak, uint8_t* seedBytes, uint32_t addressIndex);
	~Signing() { /* no seed to delete */ }
	bool sign(Transaction* tx, tryte_t* bundleHashFragment);
};


#endif // SIGNING_H
