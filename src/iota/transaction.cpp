/*
 * transaction.cpp
 *
 *  Created on: 03.03.2019
 *      Author: thomas
 */

#include <string.h>
#include <assert.h>
#include "iota/transaction.h"
#include "iota/addresses.h"
#include "aux.h"

extern Conversion* pConversion;

Transaction::Transaction() {
	//init();
}

void Transaction::init(TX_t* p) {
	m_charTX = p;
	m_addressIndex = 0xffffffff;
	m_charTX->m_zeroterminator = 0;	// zero-terminator for convenience
	m_next = (Transaction*) 0;
	memset(m_charTX->m_signatureMessageFragment, 0, sizeof(m_charTX->m_signatureMessageFragment));
	memset(m_charTX->m_address, '9', sizeof(m_charTX->m_address));
	m_value = 0;
	memset(m_charTX->m_obsoleteTag, '9', sizeof(m_charTX->m_obsoleteTag));
	m_timestamp = 0;
	m_currentIndex = 0;
	m_lastIndex = 0;
	memset(m_charTX->m_bundle, '9', sizeof(m_charTX->m_bundle));
	memset(m_charTX->m_trunkTransaction, '9', sizeof(m_charTX->m_trunkTransaction));
	memset(m_charTX->m_branchTransaction, '9', sizeof(m_charTX->m_branchTransaction));
	memset(m_charTX->m_tag, '9', sizeof(m_charTX->m_tag));
	m_attachmentTimestamp = 0;
	m_attachmentTimestampLowerBound = 0;
	m_attachmentTimestampUpperBound = 0;
	memset(m_charTX->m_nonce, '9', sizeof(m_charTX->m_nonce));
	m_prev = 0;
	m_next = 0;
	m_type = Transaction::Empty;
}

void Transaction::convertIntValues()
{
    pConversion->s64_to_chars(m_value, m_charTX->m_value, sizeof(m_charTX->m_value));
    pConversion->u32_to_chars(m_timestamp, m_charTX->m_timestamp, sizeof(m_charTX->m_timestamp));
    pConversion->u32_to_chars(m_currentIndex, m_charTX->m_currentIndex, sizeof(m_charTX->m_currentIndex));
    pConversion->u32_to_chars(m_lastIndex, m_charTX->m_lastIndex, sizeof(m_charTX->m_lastIndex));
    // is set after PoW
/*    int64_to_chars(m_attachmentTimestamp, m_charTX->m_attachmentTimestamp, 9);
    int64_to_chars(m_attachmentTimestampLowerBound, m_charTX->m_attachmentTimestampLower, 9);
    int64_to_chars(m_attachmentTimestampUpperBound, m_charTX->m_attachmentTimestampUpper, 9);*/
}


void Transaction::increment_obsolete_tag(unsigned int tag_increment)
{
    char extended_tag[81];
    unsigned char tag_bytes[48];
    rpad_chars(extended_tag, m_charTX->m_obsoleteTag, NUM_HASH_TRYTES);
    pConversion->chars_to_bytes(extended_tag, tag_bytes, NUM_HASH_TRYTES);

    pConversion->bytes_add_u32_mem(tag_bytes, tag_increment);
    pConversion->bytes_to_chars(tag_bytes, extended_tag, 48);

    memcpy(m_charTX->m_obsoleteTag, extended_tag, 27);
}

void Transaction::setMessage(char* msg) {
	rpad_chars(m_charTX->m_signatureMessageFragment, msg, sizeof(m_charTX->m_signatureMessageFragment));
}
void Transaction::setAddress(char* addr) {
	memcpy(m_charTX->m_address, addr, 81);
}

void Transaction::setAddressIndex(uint32_t idx) {
	m_addressIndex = idx;
}

void Transaction::setValue(int64_t value) {
	m_value = value;
}
void Transaction::setObsoleteTag(char* tag) {
	rpad_chars(m_charTX->m_obsoleteTag, tag, sizeof(m_charTX->m_obsoleteTag));
}
void Transaction::setTimestamp(uint32_t timestamp) {
	m_timestamp = timestamp;
}
void Transaction::setCurrentIndex(uint32_t index) {
	m_currentIndex = index;
}
void Transaction::setLastIndex(uint32_t index) {
	m_lastIndex = index;

}

void Transaction::setTag(char* tag) {
	rpad_chars(m_charTX->m_tag, tag, 27);
}

// following after / during pow
void Transaction::setTrunkTransaction(char* trunk) {
	rpad_chars(m_charTX->m_trunkTransaction, trunk, sizeof(m_charTX->m_trunkTransaction));
}
void Transaction::setBranchTransaction(char* branch) {
	rpad_chars(m_charTX->m_branchTransaction, branch, sizeof(m_charTX->m_branchTransaction));
}

void Transaction::setAttachmentTimestamp(int64_t timestamp) {
	pConversion->s64_to_chars(timestamp, m_charTX->m_attachmentTimestamp, sizeof(m_charTX->m_attachmentTimestamp));
}

void Transaction::setAttachmentTimestampLowerBound(int64_t timestamp) {
	pConversion->s64_to_chars(timestamp, m_charTX->m_attachmentTimestampLower, sizeof(m_charTX->m_attachmentTimestampLower));
}
void Transaction::setAttachmentTimestampUpperBound(int64_t timestamp) {
	(void) timestamp;	// surpress unused
	memcpy(m_charTX->m_attachmentTimestampUpper, "K99999999", sizeof(m_charTX->m_attachmentTimestampUpper));
	// todo ... conversion gives wrong chars
	//conv->s64_to_chars(timestamp, m_charTX->m_attachmentTimestampUpper, sizeof(m_charTX->m_attachmentTimestampUpper));
}

void Transaction::setNonce(char* nonce) {
	memcpy(m_charTX->m_nonce, nonce, sizeof(m_charTX->m_nonce));
}

void Transaction::setBundle(char* bundle) {
	memcpy(m_charTX->m_bundle, bundle, sizeof(m_charTX->m_bundle));
}
/*
void Transaction::setTimestampChars(char* timestamp) {
	memcpy(m_charTX->m_timestamp, timestamp, 9);
}

void Transaction::setLastIndexChars(char* index) {
	memcpy(m_charTX->m_lastIndex, index, 9);
}

void Transaction::setValueChars(char* value) {
	memcpy(m_charTX->m_value, value, 27);
}

*/
