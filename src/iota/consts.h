/*
 * consts.h
 *
 *  Created on: 23.02.2019
 *      Author: thomas
 */

#ifndef IOTA_CONSTS_H_
#define IOTA_CONSTS_H_

#define minTimestampTrytes 						"999999999"
#define maxTimestampTrytes 						"L99999999"

#define SignatureMessageFragmentTrinaryOffset 0
#define SignatureMessageFragmentTrinarySize   6561
#define AddressTrinaryOffset                   (SignatureMessageFragmentTrinaryOffset + SignatureMessageFragmentTrinarySize)
#define AddressTrinarySize                     243
#define ValueOffsetTrinary                     (AddressTrinaryOffset + AddressTrinarySize)
#define ValueSizeTrinary                       81
#define ObsoleteTagTrinaryOffset               (ValueOffsetTrinary + ValueSizeTrinary)
#define ObsoleteTagTrinarySize                 81
#define TimestampTrinaryOffset                 (ObsoleteTagTrinaryOffset + ObsoleteTagTrinarySize)
#define TimestampTrinarySize                   27
#define CurrentIndexTrinaryOffset              (TimestampTrinaryOffset + TimestampTrinarySize)
#define CurrentIndexTrinarySize                27
#define LastIndexTrinaryOffset                 (CurrentIndexTrinaryOffset + CurrentIndexTrinarySize)
#define LastIndexTrinarySize                   27
#define BundleTrinaryOffset                    (LastIndexTrinaryOffset + LastIndexTrinarySize)
#define BundleTrinarySize                      243
#define TrunkTransactionTrinaryOffset          (BundleTrinaryOffset + BundleTrinarySize)
#define TrunkTransactionTrinarySize            243
#define BranchTransactionTrinaryOffset         (TrunkTransactionTrinaryOffset + TrunkTransactionTrinarySize)
#define BranchTransactionTrinarySize           243
#define TagTrinaryOffset                       (BranchTransactionTrinaryOffset + BranchTransactionTrinarySize)
#define TagTrinarySize                         81
#define AttachmentTimestampTrinaryOffset       (TagTrinaryOffset + TagTrinarySize)
#define AttachmentTimestampTrinarySize         27

#define AttachmentTimestampLowerBoundTrinaryOffset  (AttachmentTimestampTrinaryOffset + AttachmentTimestampTrinarySize)
#define AttachmentTimestampLowerBoundTrinarySize    27
#define AttachmentTimestampUpperBoundTrinaryOffset  (AttachmentTimestampLowerBoundTrinaryOffset + AttachmentTimestampLowerBoundTrinarySize)
#define AttachmentTimestampUpperBoundTrinarySize    27
#define NonceTrinaryOffset                          (AttachmentTimestampUpperBoundTrinaryOffset + AttachmentTimestampUpperBoundTrinarySize)
#define NonceTrinarySize                            81

#define TransactionTrinarySize (SignatureMessageFragmentTrinarySize + AddressTrinarySize + \
	ValueSizeTrinary + ObsoleteTagTrinarySize + TimestampTrinarySize + \
	CurrentIndexTrinarySize + LastIndexTrinarySize + BundleTrinarySize + \
	TrunkTransactionTrinarySize + BranchTransactionTrinarySize + \
	TagTrinarySize + AttachmentTimestampTrinarySize + \
	AttachmentTimestampLowerBoundTrinarySize + AttachmentTimestampUpperBoundTrinarySize + \
	NonceTrinarySize)

#define SignatureMessageFragmentSizeInTrytes (SignatureMessageFragmentTrinarySize / 3)
#define TransactionTrytesSize                (TransactionTrinarySize / 3)


#endif /* IOTA_CONSTS_H_ */
